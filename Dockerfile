FROM node:14-alpine
WORKDIR /opt/usr/deveps
COPY * ./
RUN npm install
EXPOSE 8888
CMD ["npm","start"]